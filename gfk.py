import urllib2
import fileinput

url = 'http://ecpo-se.gfk.com/dynaload_extrec.aspx?pageid=extrec'

xml = ''
for line in fileinput.input():
    xml += line

req = urllib2.Request(url)
req.add_data(xml)
resp = urllib2.urlopen(req)

print "Response:"
print resp.read()
