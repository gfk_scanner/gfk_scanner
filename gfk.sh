#/bin/sh
cd "$(dirname "$0")"

dev=$(dmesg | grep "opticon converter now attached to" | tail -n 1 | awk '{print $NF}')

if python opticon.py /dev/$dev > gfk2.xml
then
    /bin/mv gfk2.xml gfk.xml
fi
python gfk.py < gfk.xml
